﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace testing_game_platform.UnitTests.Services
{
    public class Update_Rating
    {

        [Fact]
        public void Non_Existing_Rating_Cannot_Be_Updated()
        {
            Assert.Equal(null, "hans");
        }

        [Fact]
        public void Changing_A_Field_Value_Results_In_Updated()
        {
            Assert.Equal(null, "hans");
        }
    }
}
