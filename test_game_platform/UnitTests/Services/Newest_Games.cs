﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using NFluent;
using game_platform.Game.Database;
using game_platform.Game;

namespace testing_game_platform.UnitTests.Services
{
    public class Newest_Games
    {
        [Fact]
        public void Empty_List_Of_Games_Should_Return_Empty_List_For_Newest_Games()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> empty = new List<Game>();

            mockRepository.Setup(x => x.GetAll()).Returns(empty);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetNewestGames(0);

            Assert.Equal(empty, result);
        }

        [Fact]
        public void Games_With_Pair_Wise_Different_PublicationDate_Are_Sorted_Descending_PublicationDate()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> games = new List<Game> {
                new Game {  Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { Id = 2, Title = "FIFA 20 ", PublicationDate = new DateTime(2020, 10, 03), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }
            };

            mockRepository.Setup(x => x.GetAll()).Returns(games);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetMostPopularGames(0);
            var scores = result.Select(x => x.PublicationDate).ToList();
            Check.That(scores).IsInDescendingOrder();
        }

        [Fact]
        public void Games_With_Equal_PublicationDate_Should_Be_Sorted_Alphabetically_By_Title()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> games = new List<Game> {
                new Game {  Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { Id = 2, Title = "FIFA 20 ", PublicationDate = new DateTime(2009, 10, 03), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { Id = 3, Title = "Zebra", PublicationDate = new DateTime(2009, 10, 03), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }

            };

            mockRepository.Setup(x => x.GetAll()).Returns(games);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetMostPopularGames(0);
            var scores = result.Select(x => x.PublicationDate).ToList();
            var titles = result.Select(x => x.Title).ToList();
            Check.That(scores).IsInDescendingOrder();
            Check.That(titles).IsInAscendingOrder();
        }

    }
}
