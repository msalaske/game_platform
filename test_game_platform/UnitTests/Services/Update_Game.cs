﻿using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using NFluent;
using System.Linq;
using game_platform.Game;
using game_platform.Game.Database;
using game_platform.Game.Exceptions;

namespace testing_game_platform.UnitTests.Services
{


    // TODO: Test what happens if new Platforms are added to a Game / alternative: platforms are fixed can only be inserted once.
    // TODO: replace PlatformEntity with new onces, delete not used once.
    public class Update_Game
    {
        [Fact]
        public void Non_Existing_Game_Cannot_Be_Updated()
        {
            Mock<IGameRepository> gameRepository = new Mock<IGameRepository>();
            Game game = new Game { Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games 2", Description = "online game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };

            GameService gameService = new GameService(gameRepository.Object);

            Assert.Throws<ArgumentNullException>(() => gameService.UpdateGame(game));
        }

        [Fact]
        public void Changing_To_Title_Which_Not_Exists_Yet_Results_In_Update()
        {

            Mock<IGameRepository> gameRepository = new Mock<IGameRepository>();

            Game previousGame = new Game { Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games 2", Description = "online game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };

            gameRepository.Setup(x => x.GetAll()).Returns(new List<Game> { previousGame });

            gameRepository.Setup(x => x.Insert(previousGame)).Returns(previousGame);

            GameService gameService = new GameService(gameRepository.Object);

            gameRepository.Setup(x => x.GetAll()).Returns(new List<Game> { previousGame});

            var result0 = gameService.GetAllGames();
            Check.That(result0.Select(x => x.Title)).ContainsExactly("League of Legends");


            previousGame.Title = "Mr. X";
            gameRepository.Setup(x => x.Update(previousGame)).Returns(previousGame);

            var result = gameService.GetAllGames();
            Check.That(result.Select(x => x.Title)).ContainsExactly("Mr. X");
        }

        [Fact]
        public void Changing_To_Title_Which_Already_Exists_Does_Not_Result_In_Update()
        {

            Mock<IGameRepository> gameRepository = new Mock<IGameRepository>();
            Game game = new Game { Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games 2", Description = "online game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Game game2 = new Game { Id = 2, Title = "Mr. X", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games 2", Description = "online game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };

            gameRepository.Setup(x => x.Insert(game)).Returns(game);
            gameRepository.Setup(x => x.Insert(game2)).Returns(game2);
            gameRepository.Setup(x => x.GetAll()).Returns(new List<Game> { game, game2 } );
            game.Title = "Mr.X";
            gameRepository.Setup(x => x.Update(game)).Returns(game);
            GameService gameService = new GameService(gameRepository.Object);

            Assert.Throws<MultipleGamesWithSameTitleNotAllowedExcpetion>(() => gameService.UpdateGame(game));

        }

    }
}

