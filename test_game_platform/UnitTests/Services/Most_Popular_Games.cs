﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using NFluent;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Linq;
using game_platform.Game;
using game_platform.Game.Database;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace testing_game_platform.UnitTests.Services
{
    public class Most_Popular_Games
    {


        [Fact]
        public void Filter_By_PC_Returns_Most_Popular_Games_Of_Platform_PC()
        {
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> pcGames = new List<Game> {
               new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } }
            };

            mockRepository.Setup(x => x.GetGamesByPlatform(4)).Returns(pcGames);
            GameService gameService = new GameService(mockRepository.Object);

            IList<Game> sortedPcGames = new List<Game> {
                new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.PC } } }
            };
            var result = gameService.GetMostPopularGames(4);

            Check.That(sortedPcGames.Select(x => x.Id).ToList()).ContainsExactly(result.Select(x => x.Id).ToList());
        }

        [Fact]
        public void Filter_By_Playstation_Returns_Most_Popular_Games_Of_Platform_Playstation()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> playstationGames = new List<Game> {
               new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } }
            };

            mockRepository.Setup(x => x.GetGamesByPlatform(2)).Returns(playstationGames);
            GameService gameService = new GameService(mockRepository.Object);
            
            IList<Game> sortedPlaystationGames = new List<Game> {
                new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Playstation } } }
            };
            var result = gameService.GetMostPopularGames(2);

            Check.That(sortedPlaystationGames.Select(x => x.Id).ToList()).ContainsExactly(result.Select(x => x.Id).ToList());
        }
 

        [Fact]
        public void Filter_By_XBOX_Returns_Most_Popular_Games_Of_XBOX()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> xboxGames = new List<Game> {
               new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }
            };

            mockRepository.Setup(x => x.GetGamesByPlatform(4)).Returns(xboxGames);
            GameService gameService = new GameService(mockRepository.Object);

            IList<Game> sortedXboxGames = new List<Game> {
                new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }
            };
            var result = gameService.GetMostPopularGames(4);

            Check.That(sortedXboxGames.Select(x => x.Id).ToList()).ContainsExactly(result.Select(x => x.Id).ToList());
        }

        [Fact]
        public void Filter_By_Nintendo_Returns_Most_Popular_Games_Of_Nintendo()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> nintendoGames = new List<Game> {
               new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } }
            };

            mockRepository.Setup(x => x.GetGamesByPlatform(3)).Returns(nintendoGames);
            GameService gameService = new GameService(mockRepository.Object);

            IList<Game> sortedNintendoGames = new List<Game> {
                new Game { AverageScore = 15, Id = 2, Title = "D", PublicationDate = new DateTime(2020, 10, 10), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 13, Id = 4, Title = "B", PublicationDate = new DateTime(2020, 10, 13), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo} } },
                new Game { AverageScore = 12, Id = 5, Title = "C", PublicationDate = new DateTime(2020, 10, 14), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 12, Id = 3, Title = "A", PublicationDate = new DateTime(2020, 10, 12), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } },
                new Game { AverageScore = 11, Id = 6, Title = "D", PublicationDate = new DateTime(2020, 10, 15), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } }
            };
            var result = gameService.GetMostPopularGames(3);
            Check.That(sortedNintendoGames.Select(x => x.Id).ToList()).ContainsExactly(result.Select(x => x.Id).ToList());
        }


        [Fact]
        public void Empty_List_Of_Games_Should_Return_Empty_List_For_Most_Popular_Games()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> empty = new List<Game>();

            mockRepository.Setup(x => x.GetAll()).Returns(empty);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetMostPopularGames(0);

            Assert.Equal(empty, result);
        }

        [Fact]
        public void Games_Sorted_Descending_AverageScore_And_ThenDescending_PublicationDate()
        {


            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> games = new List<Game> { 
                new Game { AverageScore = 9, Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 4, Id = 2, Title = "FIFA 20 ", PublicationDate = new DateTime(2020, 10, 03), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }
            };

            mockRepository.Setup(x => x.GetAll()).Returns(games);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetMostPopularGames(0);
            var scores = result.Select(x => x.AverageScore).ToList();
            Check.That(scores).IsInDescendingOrder();
        }
        [Fact]
        public void Games_With_Equal_AverageScore_Are_Sorted_Descending_PublicationDate()
        {


            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> games = new List<Game> {
                new Game { AverageScore = 4, Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { AverageScore = 4, Id = 2, Title = "FIFA 20 ", PublicationDate = new DateTime(2020, 10, 03), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }
            };

            mockRepository.Setup(x => x.GetAll()).Returns(games);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetMostPopularGames(0);
            var scores = result.Select(x => x.AverageScore).ToList();
            var publicationDates = result.Select(x => x.PublicationDate).ToList();

            Check.That(publicationDates).IsInDescendingOrder();
        }
        [Fact]
        public void Games_With_No_Rating_Should_Not_Be_Incorporated_In_Popular_Games()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            IList<Game> games = new List<Game> {
                new Game { AverageScore = 4, Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } },
                new Game { Id = 2, Title = "FIFA 20 ", PublicationDate = new DateTime(2020, 10, 03), Producer = "Electronic Arts", Description = "football game", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } }
            };

            mockRepository.Setup(x => x.GetAll()).Returns(games);
            GameService gameService = new GameService(mockRepository.Object);

            var result = gameService.GetMostPopularGames(0);
            Check.That(result).Not.Contains(games[1]);

        }

    }
}
