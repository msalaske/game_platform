﻿using game_platform.ExceptionHandling;
using Moq;
using System;
using System.Collections.Generic;
using game_platform.Game;
using game_platform.Game.Database;
using Xunit;

namespace testing_game_platform.UnitTests.Service
{
    public class Add_Game
    {
        [Fact]
        public void No_Entry_Fields_Filled_Out_Should_Return_Exception()
        {
            // Arrange 
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();
            Game game = null;
            GameService gameService = new GameService(mockRepository.Object);

            // Assert
            Assert.Throws<ArgumentNullException>(() => gameService.AddGame(game));
        }

        [Fact]
        public void If_All_Requiered_Fields_Filled_Out_Record_Is_Inserted()
        {
            // Arrange 
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();
       
            Game game = new Game { Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena" };
            mockRepository.Setup(x => x.Insert(game)).Returns(game);

            GameService gameService = new GameService(mockRepository.Object);

            // Act

            var result = gameService.AddGame(game);
            // Assert
            Assert.Equal(game, result);
        }

        [Fact]
        public void Not_All_Requiered_Fields_Filled_Out_Returns_Exception()
        {
            // Arrange 
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            Game game = new Game { Title = "League of Legends"};
            mockRepository.Setup(x => x.Insert(game)).Throws<NotAllRequiredFieldsFilledOutException>();
            GameService gameService = new GameService(mockRepository.Object);

            // Assert
            Assert.Throws<NotAllRequiredFieldsFilledOutException>(() => gameService.AddGame(game));

        }

        [Fact]
        public void Given_Platform_For_A_Game_Inserts_Platform_And_Game()
        {
            // Arrange 
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();
            Game game = new Game { Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform>{ new Platform { Name = Platform.PlatformType.XBOX } } };
            mockRepository.Setup(x => x.Insert(game)).Returns(game);

            GameService gameService = new GameService(mockRepository.Object);

            // Act
            var result = gameService.AddGame(game);

            // Assert
            Assert.Equal(game, result);

        }

        [Fact]
        public void Game_With_Id_Which_Already_Exists_Cannot_Be_Inserted_Due_To_Duplication()
        {

            // Arrange 
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();
            Game game = new Game { Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Game game2 = new Game { Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            mockRepository.Setup(x => x.Insert(game)).Returns(game);
            mockRepository.Setup(x => x.GetById(1)).Returns(game);

            GameService gameService = new GameService(mockRepository.Object);

            // Assertion
            Assert.Throws<EntityAlreadyExistsException>(() => gameService.AddGame(game2));
        }

    }
}
