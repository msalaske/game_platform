﻿using game_platform.ExceptionHandling;
using Moq;
using System;
using game_platform.Game;
using game_platform.Game.Database;
using Xunit;

namespace testing_game_platform.UnitTests.Services
{
    public class Delete_Game
    {
        [Fact]
        public void A_Non_Existing_Game_Cannot_Be_Deleted()
        {
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();
            GameService gameService = new GameService(mockRepository.Object);
            // Assert
            Assert.Throws<EntityDoesNotExistException>(() => gameService.DeleteGameById(1));
        }

        [Fact]
        public void A_Game_With_Existing_Id_Should_Be_Deleted_If_Wished()
        {
            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            Game game = new Game { Id = 1, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena" };
            mockRepository.Setup(x => x.Insert(game)).Returns(game);
            mockRepository.Setup(x => x.GetById(1)).Returns(game);
            mockRepository.Setup(x => x.Delete(1)).Returns(game);

            GameService gameService = new GameService(mockRepository.Object);
            var result = gameService.DeleteGameById(1);

            // Assert
            Assert.Equal(game, result);

        }

    }
}
