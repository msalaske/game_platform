﻿using System;
using System.Collections.Generic;
using System.Text;
using game_platform.Game;
using game_platform.Game.Database;
using Xunit;
using Moq;

namespace testing_game_platform.UnitTests.Services
{
    public class Get_All_Games_Associated_With_Platform
    {
        [Fact]
        public void Should_Return_Empty_Game_List_For_A_Platform_With_No_Assigned_Games()
        {

            Mock<IPlatformRepository> platformRepository = new Mock<IPlatformRepository>();
            PlatformService platformService = new PlatformService(platformRepository.Object);
            Platform platform = new Platform { Id = 1, Name = Platform.PlatformType.PC};
            platformRepository.Setup(x => x.GetById(1)).Returns(platform);
            Platform result = platformService.GetAllGamesAssociatedWithPlatform(1);

            Assert.Equal(platform.Games, result.Games);

        }
        [Fact]
        public void Should_Return_List_Of_Games_For_A_Given_Platform_If_Assignment_Exists()
        {

            Mock<IPlatformRepository> platformRepository = new Mock<IPlatformRepository>();
            PlatformService platformService = new PlatformService(platformRepository.Object);
            IList<Game> games =  new List<Game> {new Game { Title = "League of Legends" } };
            Platform platform = new Platform { Id = 1, Name = Platform.PlatformType.PC, Games = games };
            platformRepository.Setup(x => x.GetById(1)).Returns(platform);
            Platform result = platformService.GetAllGamesAssociatedWithPlatform(1);

            Assert.Equal(games, result.Games);

        }

        [Fact]
        public void Games_With_Multiple_Platforms_Assigned_Occur_In_List_For_Every_Platform()
        {

            Mock<IPlatformRepository> platformRepository = new Mock<IPlatformRepository>();
            PlatformService platformService = new PlatformService(platformRepository.Object);
            IList<Game> games = new List<Game> { new Game { Title = "League of Legends" } };
            Platform platform = new Platform { Id = 1, Name = Platform.PlatformType.PC, Games = games };
            Platform platform2 = new Platform { Id = 2, Name = Platform.PlatformType.Nintendo, Games = games };

            platformRepository.Setup(x => x.GetById(1)).Returns(platform);
            platformRepository.Setup(x => x.GetById(2)).Returns(platform2);

            Platform result = platformService.GetAllGamesAssociatedWithPlatform(1);
            Platform result2 = platformService.GetAllGamesAssociatedWithPlatform(2);
            Assert.Equal(games, result.Games);
            Assert.Equal(games, result2.Games);

        }

    }
}
