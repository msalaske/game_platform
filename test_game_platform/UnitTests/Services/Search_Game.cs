﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using game_platform.Game;
using game_platform.Game.Database;
using Xunit;

namespace testing_game_platform.UnitTests.Services
{
    public class Search_Game
    {
        [Fact]
        public void An_Empty_Search_String_Results_To_An_Empty_List_Of_Query_Results()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            GameService gameService = new GameService(mockRepository.Object);

            IList<Game> result = gameService.SearchbyString("");

            Assert.Equal(null, result);
        }

        [Fact]
        public void Query_Results_Contain_All_Games_Containing_Search_String()
        {

            Mock<IGameRepository> mockRepository = new Mock<IGameRepository>();

            GameService gameService = new GameService(mockRepository.Object);
            Game game = new Game { Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Game game2 = new Game { Title = "Test", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "shooter", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.Nintendo } } };

            mockRepository.Setup(x => x.Insert(game)).Returns(game);
            mockRepository.Setup(x => x.Insert(game2)).Returns(game2);

            IList<Game> expected = new List<Game>(new Game[] { game });
            mockRepository.Setup(x => x.Search("League")).Returns(expected);

            IList<Game> result = gameService.SearchbyString("League");

            Assert.Equal(expected, result);
        }

    }
}