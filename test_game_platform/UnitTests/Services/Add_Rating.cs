﻿using game_platform.ExceptionHandling;
using Moq;
using System;
using System.Collections.Generic;
using game_platform.Game;
using game_platform.Game.Exceptions;
using Xunit;
using game_platform.Rating.Database;
using game_platform.Rating;

namespace testing_game_platform.UnitTests.Service
{
    public class Add_Rating
    {
        //TODO: Fix texts, that they cover new Rating modeling (including RatingCategories).
        
        /*
        [Fact]
        public void A_Rating_With_Score_Not_In_Range_1_Until_5_Cannot_Be_Inserted()
        {
            // Arrange
            Mock<IRatingRepository> mockRepository = new Mock<IRatingRepository>();
            Mock<IGameService> mockGameService = new Mock<IGameService>();

            Game game = new Game {Id = 2, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Rating rating = new Rating { Score = 6, Comment = new Comment { Subject = "Top", Message = "I like this game"}, GameId = 2};

            mockGameService.Setup(x => x.AddGame(game)).Returns(game);

            RatingService ratingService = new RatingService(mockGameService.Object, mockRepository.Object);

            Assert.Throws<NotValidScoreRangeException>(() => ratingService.AddRating(rating));
        }
       
        [Fact]
        public void A_Rating_Without_Assigment_To_A_Game_Is_Not_Possible()
        {

            // Arrange
            Mock<IRatingRepository> mockRepository = new Mock<IRatingRepository>();
            Mock<IGameService> mockGameService = new Mock<IGameService>();

            Game game = new Game { Id = 2, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Rating rating = new Rating { Score = 2, Comment = new Comment { Subject = "Top", Message = "I like this game" } };

            mockGameService.Setup(x => x.AddGame(game)).Returns(game);

            RatingService ratingService = new RatingService(mockGameService.Object, mockRepository.Object);

            Assert.Throws<ArgumentNullException>(() => ratingService.AddRating(rating));
        }

        [Fact]
        public void A_Rating_With_Missing_Score_Cannot_Be_Inserted()
        {

            // Arrange
            Mock<IRatingRepository> mockRepository = new Mock<IRatingRepository>();
            Mock<IGameService> mockGameService = new Mock<IGameService>();

            Game game = new Game { Id = 2, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Rating rating = new Rating { Comment = new Comment { Subject = "Top", Message = "I like this game" } };

            mockGameService.Setup(x => x.AddGame(game)).Returns(game);
            mockRepository.Setup(x => x.Insert(rating)).Returns(rating);

            RatingService ratingService = new RatingService(mockGameService.Object, mockRepository.Object);

            Assert.Throws<NotAllRequiredFieldsFilledOutException>(() => ratingService.AddRating(rating));
        }

        [Fact]
        public void A_Rating_With_Missing_Comment_But_With_A_Valid_Score_Can_Be_Inserted()
        {
            // Arrange
            Mock<IRatingRepository> mockRepository = new Mock<IRatingRepository>();
            Mock<IGameService> mockGameService = new Mock<IGameService>();

            Game game = new Game { Id = 2, Title = "League of Legends", PublicationDate = new DateTime(2009, 10, 03), Producer = "Riot Games", Description = "multiplayer online battle arena", Platforms = new List<Platform> { new Platform { Name = Platform.PlatformType.XBOX } } };
            Rating rating = new Rating() { Score = 2, GameId = 2 };

            mockGameService.Setup(x => x.AddGame(game)).Returns(game);
            mockGameService.Setup(x => x.GetGameById(2)).Returns(game);
            mockRepository.Setup(x => x.Insert(rating)).Returns(rating);

            RatingService ratingService = new RatingService(mockGameService.Object, mockRepository.Object);

            var result = ratingService.AddRating(rating);

            Assert.Equal(rating, result);
        }
         */

    }
}