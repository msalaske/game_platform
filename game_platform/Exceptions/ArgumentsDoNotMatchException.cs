﻿using System;

namespace game_platform.ExceptionHandling
{
    public class ArgumentsDoNotMatchException : Exception
    {
        public ArgumentsDoNotMatchException(){}
        public ArgumentsDoNotMatchException(string message): base(message) {}
        public ArgumentsDoNotMatchException(params object[] parameter): base($"Arguments do not match: {parameter}") {}
        public ArgumentsDoNotMatchException(string message, Exception inner): base(message, inner) { }
    }
}
