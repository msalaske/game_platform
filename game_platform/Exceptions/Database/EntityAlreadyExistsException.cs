﻿using System;

namespace game_platform.ExceptionHandling
{
    public class EntityAlreadyExistsException : Exception
    {
        public EntityAlreadyExistsException(){}
        public EntityAlreadyExistsException(string message): base(message) {}
        public EntityAlreadyExistsException(Type type, object identifier): base($"Entity of type {type} with identifier {identifier} already exists") {}
        public EntityAlreadyExistsException(string message, Exception inner): base(message, inner) {}
    }
}
