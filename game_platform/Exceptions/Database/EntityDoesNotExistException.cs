﻿using System;

namespace game_platform.ExceptionHandling
{
    public class EntityDoesNotExistException : Exception
    {
        public EntityDoesNotExistException() {}
        public EntityDoesNotExistException(string message): base(message) {}
        public EntityDoesNotExistException(Type type, object id): base($"Entity of type {type} with id {id} not found") { }
        public EntityDoesNotExistException(string message, Exception inner): base(message, inner) { }
    }
}
