﻿using System;

namespace game_platform.ExceptionHandling
{
    public class NotAllRequiredFieldsFilledOutException : Exception
    {
        public NotAllRequiredFieldsFilledOutException(){}
        public NotAllRequiredFieldsFilledOutException(string message) : base(message) {}
        public NotAllRequiredFieldsFilledOutException(string message, Exception inner): base(message, inner) {}
    }
}
