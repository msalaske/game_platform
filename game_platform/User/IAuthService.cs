﻿using game_platform.User.ResetPasswords;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace game_platform.User
{
    public interface IAuthService
    {
        Task<User> Register(RegisterUser user);
        Task<User> Login(LoginUser user);
        Task<User> ConfirmEmail(string userId, string token);
        Task<User> ResetPassword(ResetPasswordUser user);
        Task<User> GenerateMailToResetPassword(string userId);
        Task<User> GetUserById(string userId);
        ResetPasswordURIValidity CheckValidityOfResetPasswordURI(string resetPasswordURI);
    }
}
