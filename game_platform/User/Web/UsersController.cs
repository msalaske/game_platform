﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using game_platform.ExceptionHandling;
using Microsoft.AspNetCore.WebUtilities;
using System.Web;
using game_platform.User.Exceptions;
using game_platform.User.Web.Dto;

namespace game_platform.User.Web
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;

        public UsersController(IAuthService authService, IMapper mapper)
        {
            _authService = authService;
            _mapper = mapper;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Register([FromBody] RegisterUserDTO userDTO)
        {
            if (!userDTO.ConfirmedPassword.Equals(userDTO.Password))
            {
                throw new PasswordDoesNotMatchConfirmedPasswordException();
            }
            UserDTO UserDTO = _mapper.Map<UserDTO>(_authService.Register(_mapper.Map<RegisterUser>(userDTO)).Result);
            return Created("/api/users/" + UserDTO.UserId, UserDTO);
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody] LoginUserDTO userDTO)
        {
            var result = _mapper.Map<UserDTO>(_authService.Login(_mapper.Map<LoginUser>(userDTO)).Result);
            if (result == null)
            {
                throw new System.ApplicationException("Login failed!");
            }
            return Ok(result);
        }

        [HttpGet("confirmed")]
        [AllowAnonymous]
        public IActionResult ConfirmEmail([FromQuery] string userId, [FromQuery] string token)
        {
            var result = _mapper.Map<UserDTO>(_authService.ConfirmEmail(userId, token).Result);
            if (result == null)
            {
                throw new InvalidRegisterAttemptException();
            }
            return Ok(result);
        }

        [HttpPost("generateResetPasswordLink")]
        [AllowAnonymous]
        public IActionResult GenerateResetPasswordLink([FromBody] UserDTO userDTO)
        {
            var result = _mapper.Map<UserDTO>(_authService.GenerateMailToResetPassword(userDTO.UserId).Result);
            if (result == null)
            {
                throw new ResetPasswordAttemptInvalidException();
            }
            return Ok(result);
        }

        [HttpPost("resetPassword")]
        [AllowAnonymous]
        public IActionResult ResetPassword([FromBody] ResetPasswordDTO userDTO)
        {
            if (!userDTO.NewPassword.Equals(userDTO.NewPasswordConfirmed))
            {
                throw new PasswordDoesNotMatchConfirmedPasswordException();
            }
            var result = _mapper.Map<UserDTO>(_authService.ResetPassword(_mapper.Map<ResetPasswordUser>(userDTO)).Result);
            return Ok(result);
        }


        [HttpPost("checkValidityOfResetPasswordURI")]
        [AllowAnonymous]
        public IActionResult checkValidityOfResetPasswordURI([FromBody] ResetPasswordCheckValidity checkValidityDTO)
        {
            var result = _mapper.Map<ResetPasswordURIValidityDTO>(_authService.CheckValidityOfResetPasswordURI(checkValidityDTO.URI));
            return Ok(result);
        }
    }
}
