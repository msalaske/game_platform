﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User.Web.Dto
{
    public class ResetPasswordURIValidityDTO
    {
        public string URI { get; set; }
        public bool Valid { get; set; }
    }
}
