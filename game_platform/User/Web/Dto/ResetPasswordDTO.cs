﻿namespace game_platform.User.Web.Dto
{
    public class ResetPasswordDTO
    {
        public string UserId { get; set; }
        public string NewPassword { get; set; }
        public string NewPasswordConfirmed { get; set; }
        public string Token { get; set; }
    }
}
