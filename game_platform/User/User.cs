﻿using game_platform.Game;
using System.Linq;

namespace game_platform.User
{
    public class User 
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
