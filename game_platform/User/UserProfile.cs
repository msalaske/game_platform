﻿using AutoMapper;
using game_platform.User.Database;
using game_platform.User.Web.Dto;

namespace game_platform.User
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<RegisterUser, RegisterUserDTO>().ReverseMap();
            CreateMap<LoginUser, LoginUserDTO>().ReverseMap();
            CreateMap<LoginUser, UserEntity>().ReverseMap();
            CreateMap<RegisterUser, UserEntity>().ReverseMap();
            CreateMap<UserEntity, User>().ForMember(x => x.UserId, opt => opt.MapFrom(y => y.Id)).ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<ResetPasswordUser, ResetPasswordDTO>().ReverseMap();
            CreateMap<ResetPasswordURIValidityDTO, ResetPasswordURIValidity>().ReverseMap();
        }
    }
}
