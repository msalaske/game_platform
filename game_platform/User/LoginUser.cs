﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User
{
    public class LoginUser
    {
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
