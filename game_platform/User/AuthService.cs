﻿using AutoMapper;
using game_platform.ExceptionHandling;
using game_platform.User.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using System.Threading.Tasks;

namespace game_platform.User
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _authRepository;
        private readonly IEmailSender _emailSender;

        public AuthService(IAuthRepository authRepository, IEmailSender emailSender)
        {
            _authRepository = authRepository;
            _emailSender = emailSender;
        }

        public async Task<User> Login(LoginUser user)
        {
            var result = await _authRepository.PasswordSignInAsync(user);
            return result;
        }


        public async Task<User> Register(RegisterUser user)
        {
            // User wird immer hinzugefügt, wenn Registrierungsversuch. Über EmailConfirmed wird weiter gesteuert.
            User insertedUser = await _authRepository.Insert(user);

            // set up confirmation link and send email for double-opt-in
            var token = _authRepository.GenerateEmailConfirmationTokenAsync(insertedUser).Result;
            var confirmationLink = _authRepository.GenerateRegisterConfirmationLink(insertedUser, token).Result;
            await _emailSender.SendEmailAsync(user.Email, "Game platform: registration", $"Please confirm to successfully register by clicking on: {confirmationLink}");

            return insertedUser;
        }

        public async Task<User> ConfirmEmail(string userId, string Token)
        {
            User registerUser = await _authRepository.FindByIdAsync(userId);
            var userWithConfirmedRegistration = await _authRepository.ConfirmEmailAsync(registerUser, Token);
            await _authRepository.SignInAsyncDirectlyAfterRegistration(userWithConfirmedRegistration);
            return userWithConfirmedRegistration;
        }

        public async Task<User> ResetPassword(ResetPasswordUser user)
        {
            User User = await _authRepository.ResetPassword(user);
            return User;
        }

        public async Task<User> GenerateMailToResetPassword(string userId)
        {
            User user = await _authRepository.FindByIdAsync(userId);
            var token = await _authRepository.GeneratePasswordResetTokenAsync(userId);
            var link = _authRepository.GenerateResetPasswordLink(user, token).Result;
            await _emailSender.SendEmailAsync(user.Email, "Reset Password", $"Reset your password by clicking on: {link}");
            return user;
        }

        public ResetPasswordURIValidity CheckValidityOfResetPasswordURI(string resetPasswordURI)
        {
            return _authRepository.CheckValidiyOfResetPasswordURI(resetPasswordURI);
        }

        public async Task<User> GetUserById(string userId)
        {
            User user = await _authRepository.FindByIdAsync(userId);
            return user;
        }
    }
}

