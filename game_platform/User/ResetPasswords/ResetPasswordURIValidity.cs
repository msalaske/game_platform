﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User.ResetPasswords
{
    public class ResetPasswordURIValidity
    {
        public string URI { get; set; }
        public bool Valid { get; set; }
    }
}
