﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User.ResetPasswords
{
    public class ResetPasswordUser
    {
        public string UserId { get; set; }
        public string NewPassword { get; set; }
        public string Token { get; set; }
    }
}
