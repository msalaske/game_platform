﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User.Database
{
    public interface IAuthRepository
    {
        Task<User> SignInAsyncDirectlyAfterRegistration(User user);
        Task<User> PasswordSignInAsync(LoginUser user);
        Task<User> ConfirmEmailAsync(User user, string token);
        Task<User> FindByEmailAsync(string email);
        Task<User> Insert(RegisterUser user);
        Task<string> GenerateEmailConfirmationTokenAsync(User user);
        Task<string> GeneratePasswordResetTokenAsync(string userId);
        Task<string> GenerateRegisterConfirmationLink(User user, string token);
        Task<string> GenerateResetPasswordLink(User user, string token);
        Task<User> FindByIdAsync(string userId);
        Task<User> ResetPassword(ResetPasswordUser user);
        ResetPasswordURIValidity CheckValidiyOfResetPasswordURI(string URI);
    }
}
