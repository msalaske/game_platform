﻿using game_platform.Game.Subscription.Database;
using game_platform.Rating.Database;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace game_platform.User.Database
{
    public class UserEntity : IdentityUser
    {
        public UserEntity()
        {
            this.Subscriptions = new List<SubscriptionEntity>();
            this.Ratings = new List<RatingEntity>();
        }
        public ICollection<RatingEntity> Ratings { get; set; }
        public ICollection<SubscriptionEntity> Subscriptions { get; set; }
    }
}
