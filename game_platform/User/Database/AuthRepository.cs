﻿using AutoMapper;
using game_platform.Database;
using game_platform.ExceptionHandling;
using game_platform.User.Exceptions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace game_platform.User.Database
{
    public class AuthRepository : IAuthRepository
    {

        private readonly UserManager<UserEntity> _userManager;
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly DatabaseContext context;
        private readonly IMapper _mapper;

        public AuthRepository(UserManager<UserEntity> userManager, SignInManager<UserEntity> signInManager, DatabaseContext context, IMapper mapper)
        {
            this.context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public async Task<User> ConfirmEmailAsync(User user, string token)
        {
            var code = token.Replace(' ', '+');
            UserEntity userEntity = await _userManager.FindByIdAsync(user.UserId);
            IdentityResult result = await _userManager.ConfirmEmailAsync(userEntity, code);
            if (!result.Succeeded)
            {
                throw new ArgumentsDoNotMatchException(userEntity, token);
            }
            return _mapper.Map<User>(userEntity);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            UserEntity userEntity = await _userManager.FindByEmailAsync(email.Normalize());
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            return _mapper.Map<User>(userEntity);
        }

        public async Task<User> FindByIdAsync(string userId)
        {
            UserEntity userEntity = await _userManager.FindByIdAsync(userId);
            return _mapper.Map<User>(userEntity);
        }

        public async Task<string> GenerateRegisterConfirmationLink(User user, string token)
        {
            UserEntity userEntity = await _userManager.FindByIdAsync(user.UserId);
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            var callbackUrl = "http://localhost:3000/" + $"confirmRegistration?userId={userEntity.Id}&token={HttpUtility.UrlEncode(token)}";
            return callbackUrl;
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            UserEntity existingUser = await _userManager.FindByIdAsync(user.UserId);
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(existingUser);
            return token;
        }

        public async Task<User> Insert(RegisterUser user)
        {
            UserEntity existingUser = await _userManager.FindByEmailAsync(user.Email);
            if (existingUser != null)
            {
                throw new EntityAlreadyExistsException(typeof(UserEntity), user.Email);
            }
            UserEntity userEntity = _mapper.Map<UserEntity>(user);
            IdentityResult result = await _userManager.CreateAsync(userEntity, user.Password);
            if (!result.Succeeded)
            {
                throw new InvalidRegisterAttemptException();
            }
            return _mapper.Map<User>(userEntity);
        }

        public async Task<User> PasswordSignInAsync(LoginUser user)
        {
            UserEntity userEntity = await _userManager.FindByEmailAsync(user.Email);
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            SignInResult result = await _signInManager.PasswordSignInAsync(userEntity.UserName, user.Password, isPersistent: false, lockoutOnFailure: false);
            if (!result.Succeeded)
            {
                throw new InvalidLoginAttemptException("Invalid Email or Password!");
            }
            return _mapper.Map<User>(userEntity);
        }

        public async Task<User> SignInAsyncDirectlyAfterRegistration(User user)
        {
            UserEntity userEntity = await _userManager.FindByIdAsync(user.UserId);
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            userEntity.EmailConfirmed = true;
            context.Update(userEntity);
            context.SaveChanges();
            await _signInManager.SignInAsync(userEntity, isPersistent: false);
            return _mapper.Map<User>(userEntity);
        }

        public async Task<User> ResetPassword(ResetPasswordUser user)
        {
            UserEntity userEntity = await _userManager.FindByIdAsync(user.UserId);
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            var result = await _userManager.ResetPasswordAsync(userEntity, user.Token, user.NewPassword);
            if (!result.Succeeded)
            {
                throw new ResetPasswordAttemptInvalidException();
            }
            InvalidateResetPassword(userEntity.Id, user.Token);
            return _mapper.Map<User>(userEntity);
        }

        private void InvalidateResetPassword(string userId, string token)
        {
            ResetPasswordMailSent resetPasswordURI = context.ResetPasswordMailSents.Where(x => x.URI.Contains(userId) && x.URI.Contains(token)).SingleOrDefault();
            resetPasswordURI.PasswordChanged = true;
            context.Update(resetPasswordURI);
            context.SaveChanges();
        }

        public async Task<string> GeneratePasswordResetTokenAsync(string userId)
        {
            UserEntity userEntity = await _userManager.FindByIdAsync(userId);
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            var token = await _userManager.GeneratePasswordResetTokenAsync(userEntity);
            return token;
        }

        public async Task<string> GenerateResetPasswordLink(User user, string token)
        {
            UserEntity userEntity = await _userManager.FindByIdAsync(user.UserId);
            if (userEntity == null)
            {
                throw new EntityDoesNotExistException("User does not exist");
            }
            // to make this cleaner.
            var callbackUrl = "http://localhost:3000/" + $"resetPasswordForm?userId={userEntity.Id}&token={token}";
            var callbackUrlEncoded = "http://localhost:3000/" + $"resetPasswordForm?userId={userEntity.Id}&token={HttpUtility.UrlEncode(token)}";
            SaveResetPasswordURI(callbackUrl);
            return callbackUrlEncoded;
        }

        private void SaveResetPasswordURI(string uri)
        {
            ResetPasswordMailSent resetPasswordURI = new ResetPasswordMailSent(uri);
            context.Add(resetPasswordURI);
            context.SaveChanges();
        }

        public ResetPasswordURIValidity CheckValidiyOfResetPasswordURI(string URI)
        {
            ResetPasswordMailSent resetPasswordURI = context.ResetPasswordMailSents.Where(x => x.URI.Equals(URI)).SingleOrDefault();
            ResetPasswordURIValidity result = new ResetPasswordURIValidity()
            {
                URI = resetPasswordURI.URI
            };
            if (resetPasswordURI.PasswordChanged == true || DateTime.Now > resetPasswordURI.Expiration)
            {
                result.Valid = false;
            }
            else
            {
                result.Valid = true;

            }
            return result;
        }
    }
}
