﻿using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User.Database
{
    public class ResetPasswordMailSent
    {
        [Key]
        public string URI { get; set; }
        public bool PasswordChanged { get; set; } = false;
        public DateTime Expiration { get; set; } = DateTime.Now.AddHours(24);

        public ResetPasswordMailSent(string URI)
        {
            this.URI = URI;
        }
    }
}
