﻿using System;

namespace game_platform.User.Exceptions
{
    public class InvalidRegisterAttemptException : Exception
    {
        public InvalidRegisterAttemptException()
        {
        }

        public InvalidRegisterAttemptException(string message)
            : base(message) { }

        public InvalidRegisterAttemptException(string message, Exception inner)
            : base(message, inner) { }
    }
}
