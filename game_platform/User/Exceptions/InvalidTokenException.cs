﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.User.Exceptions
{
    public class InvalidTokenException : Exception
    {
        public InvalidTokenException() { }
        public InvalidTokenException(string message) : base(message) { }
        public InvalidTokenException(string message, Exception inner) : base(message, inner) { }
    }
}
