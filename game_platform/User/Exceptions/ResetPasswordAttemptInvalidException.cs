﻿using System;

namespace game_platform.User.Exceptions
{
    public class ResetPasswordAttemptInvalidException :Exception
    {
        public ResetPasswordAttemptInvalidException() {}
        public ResetPasswordAttemptInvalidException(string message) : base(message) {}
        public ResetPasswordAttemptInvalidException(string message, Exception inner): base(message, inner) { }
    }
}
