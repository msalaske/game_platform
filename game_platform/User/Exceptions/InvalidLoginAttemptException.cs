﻿using System;

namespace game_platform.User.Exceptions
{
    public class InvalidLoginAttemptException : Exception
    {
        public InvalidLoginAttemptException() { }
        public InvalidLoginAttemptException(string message) : base(message) { }
        public InvalidLoginAttemptException(string message, Exception inner) : base(message, inner) { }
    }
}
