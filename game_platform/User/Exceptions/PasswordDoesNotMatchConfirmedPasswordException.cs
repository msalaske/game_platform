﻿using System;

namespace game_platform.User.Exceptions
{
    public class PasswordDoesNotMatchConfirmedPasswordException : Exception
    {
        public PasswordDoesNotMatchConfirmedPasswordException() { }
        public PasswordDoesNotMatchConfirmedPasswordException(string message) : base(message) { }
        public PasswordDoesNotMatchConfirmedPasswordException(string message, Exception inner) : base(message, inner) { }
    }
}
