﻿using System;

namespace game_platform.User
{
    public class RegisterUser
    {
        public string Password { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}
