﻿using System;

namespace game_platform
{
    public interface IEntity
    {

        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
