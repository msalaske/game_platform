﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using game_platform.Game.Database;

namespace game_platform.Database
{
    public static class DataSeeding
    {


        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GameEntity>().HasData(new GameEntity
            {
                Id = -5,
                Title = "Counter-Strike: Global Offensive Y",
                PublicationDate = new DateTime(2012, 09, 21),
                Producer = "Valve Corporation",
                YouTubeTrailer = "https://www.youtube.com/watch?v=edYCtaNueQY",
                Description = "Counter-Strike: Global Offensive is a multiplayer first-person shooter video game developed by Valve and Hidden Path Entertainment. It is the fourth game in the Counter-Strike series and was released for Windows, OS X, Xbox 360, and PlayStation 3 in August 2012, while the Linux version was released in 2014. "
            });

            // TODO: seed more data (at least 6 games + ratings).
        }
    }
}
