﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using game_platform.Game.Database;
using static game_platform.Rating.RatingCategories.Database.RatingCategoryEntity;
using static game_platform.Game.Database.PlatformEntity;
using game_platform.Rating.Database;
using game_platform.User.Database;
using game_platform.Rating.Comment.Database;
using game_platform.Rating.RatingCategories.Database;
using game_platform.Game.Subscription.Database;

namespace game_platform.Database
{
    public class DatabaseContext : IdentityDbContext<UserEntity>

    {

        public DbSet<GameEntity> Games { get; set; }
        public DbSet<RatingEntity> Ratings { get; set; }
        public DbSet<PlatformEntity> Platforms { get; set; }
        public DbSet<PlatformGameEntity> PlatformGames { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }
        public DbSet<RatingCategoryEntity> RatingCategories { get; set; }
        public DbSet<ResetPasswordMailSent> ResetPasswordMailSents { get; set; }
        public DbSet<SubscriptionEntity> Subscriptions { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder opBuilder)

        {
            // TODO: this should be put to external file due to security.
            opBuilder.UseMySQL("server=localhost;database=testDB;uid=root;pwd=test;");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // model on creating of IdentityDbContext
            base.OnModelCreating(modelBuilder);

            // After this comment we can put customizations of Identity tables (e.g. rename the default table names)

            var length = 85;
            modelBuilder.Entity<UserEntity>(entity => entity.Property(m => m.Id).HasMaxLength(length));
            modelBuilder.Entity<UserEntity>(entity => entity.Property(m => m.NormalizedEmail).HasMaxLength(length));
            modelBuilder.Entity<UserEntity>(entity => entity.Property(m => m.NormalizedUserName).HasMaxLength(length));

            modelBuilder.Entity<IdentityRole>(entity => entity.Property(m => m.Id).HasMaxLength(length));
            modelBuilder.Entity<IdentityRole>(entity => entity.Property(m => m.NormalizedName).HasMaxLength(length));

            modelBuilder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m => m.LoginProvider).HasMaxLength(length));
            modelBuilder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m => m.ProviderKey).HasMaxLength(length));
            modelBuilder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m => m.UserId).HasMaxLength(length));
            modelBuilder.Entity<IdentityUserRole<string>>(entity => entity.Property(m => m.UserId).HasMaxLength(length));

            modelBuilder.Entity<IdentityUserRole<string>>(entity => entity.Property(m => m.RoleId).HasMaxLength(length));

            modelBuilder.Entity<IdentityUserToken<string>>(entity => entity.Property(m => m.UserId).HasMaxLength(length));
            modelBuilder.Entity<IdentityUserToken<string>>(entity => entity.Property(m => m.LoginProvider).HasMaxLength(length));
            modelBuilder.Entity<IdentityUserToken<string>>(entity => entity.Property(m => m.Name).HasMaxLength(length));

            modelBuilder.Entity<IdentityUserClaim<string>>(entity => entity.Property(m => m.Id).HasMaxLength(length));
            modelBuilder.Entity<IdentityUserClaim<string>>(entity => entity.Property(m => m.UserId).HasMaxLength(length));
            modelBuilder.Entity<IdentityRoleClaim<string>>(entity => entity.Property(m => m.Id).HasMaxLength(length));
            modelBuilder.Entity<IdentityRoleClaim<string>>(entity => entity.Property(m => m.RoleId).HasMaxLength(length));

            modelBuilder.Entity<RatingEntity>(entity => entity.Property(m => m.UserId).HasMaxLength(length));

            // Tables, considering business logic excluding security aspects and .NET Core Identity generated tables

            modelBuilder
                .Entity<PlatformEntity>()
                .Property(p => p.Name)
                .HasConversion<string>();

            modelBuilder.Entity<PlatformGameEntity>()
              .HasOne(x => x.Platform)
              .WithMany(x => x.PlatformGames);

            modelBuilder.Entity<PlatformGameEntity>()
              .HasOne(x => x.Game)
              .WithMany(x => x.PlatformGames);


            // subscriptions data base model many to many table
            modelBuilder.Entity<SubscriptionEntity>()
              .HasOne(x => x.ObservableGame)
              .WithMany(x => x.Subscriptions)
              .HasForeignKey(x => x.GameId);

            modelBuilder.Entity<SubscriptionEntity>()
              .HasOne(x => x.SubscribingUser)
              .WithMany(x => x.Subscriptions)
              .HasForeignKey(x => x.UserId);
            

            modelBuilder
                .Entity<RatingCategoryEntity>()
                .Property(p => p.Name)
                .HasConversion<string>();

            // comment is optional
            modelBuilder.Entity<CommentEntity>()
                         .HasOne(r => r.Rating)
                         .WithOne()
                         .HasForeignKey<CommentEntity>(x => new { x.RatingGameId, x.RatingUserId });


            modelBuilder.Entity<RatingEntity>()
                        .HasOne(x => x.User)
                        .WithOne()
                        .HasPrincipalKey<RatingEntity>(x => new { x.UserId, x.GameId });

            modelBuilder.Entity<RatingEntity>()
                       .HasOne(x => x.Game)
                       .WithOne()
                       .HasPrincipalKey<RatingEntity>(x => new { x.GameId, x.UserId });

            // define a composite primary key. A user can only give one rating to a specific game.
            modelBuilder.Entity<RatingEntity>().HasKey(x => new { x.GameId, x.UserId });

            // data seeding at startup
            modelBuilder.Seed();
        }
    }
}
