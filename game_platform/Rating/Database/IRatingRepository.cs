﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.Database
{
    public interface IRatingRepository
    {
        Rating Delete(long gameId, string userId);

        Rating Insert(Rating rating);

        Rating Update(Rating rating);

        IList<Rating> GetAll();

        Rating GetById(long gameId, string userId);
    }
}
