﻿using AutoMapper;
using game_platform.ExceptionHandling;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using game_platform.Game.Database;
using game_platform.Database;
using game_platform.Rating.Exceptions;
using game_platform.User.Database;
using game_platform.Rating.Comment.Database;
using game_platform.Rating.RatingCategories.Database;

namespace game_platform.Rating.Database
{
    public class RatingRepository : IRatingRepository
    {


        private readonly IMapper _mapper;
        private readonly DatabaseContext context;

        public RatingRepository(DatabaseContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }

        public Rating Insert(Rating rating)
        {
            if (!context.Games.Any(v => v.Id == rating.GameId)) throw new EntityDoesNotExistException(typeof(GameEntity), rating.GameId);
            if (!context.Users.Any(v => v.Id.Equals(rating.UserId))) throw new EntityDoesNotExistException(typeof(UserEntity), rating.UserId);
            if (context.Ratings.Where(x => x.GameId == rating.GameId && x.UserId == rating.UserId).Any()) throw new OnlyOneRatingPerGameAllowedException(rating.GameId, rating.UserId);

            GameEntity game = context
                .Games
                .Where(p => p.Id == rating.GameId).Include(v => v.Ratings)
                .SingleOrDefault();
            UserEntity user = context
                .Users
                .Where(x => x.Id.Equals(rating.UserId)).Include(v => v.Ratings)
                .SingleOrDefault();

            RatingEntity ratingEntity = new RatingEntity(game.Id, user.Id);
            game.Ratings.Add(ratingEntity);
            context.SaveChanges();

            if (rating.Comment != null)
            {
                var commentEntity = _mapper.Map<CommentEntity>(rating.Comment);
                commentEntity.Rating = ratingEntity;
                context.Comments.Add(commentEntity);
            }
            foreach (var category in rating.RatingCategories)
            {
                var entityCategory = _mapper.Map<RatingCategoryEntity>(category);
                entityCategory.Rating = ratingEntity;
                context.RatingCategories.Add(entityCategory);
            }
            context.SaveChanges();
            return rating;
        }

        public Rating GetById(long gameId, string userId)
        {
            Rating rating = _mapper.Map<Rating>(context.Ratings.Where(x => x.GameId == gameId && x.UserId == userId));
            return rating;
        }

        public IList<Rating> GetAll()
        {
            IList<Rating> ratings = _mapper.Map<IList<Rating>>(context.Ratings.ToList());
            return ratings;
        }

        public Rating Delete(long gameId, string userId)
        {
            if (gameId == 0 || string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException("entity");

            RatingEntity entity = context.Ratings.SingleOrDefault();
            context.Ratings.Remove(entity);
            context.SaveChanges();
            return _mapper.Map<Rating>(entity);
        }

        public Rating Update(Rating rating)
        {
            RatingEntity entity = _mapper.Map<RatingEntity>(rating);

            if (entity == null) throw new ArgumentNullException("entity");
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
            return _mapper.Map<Rating>(entity);
        }
    }
}
