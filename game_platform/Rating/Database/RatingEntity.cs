﻿using System.Collections.Generic;
using game_platform.Game.Database;
using game_platform.Rating.RatingCategories.Database;
using game_platform.User.Database;

namespace game_platform.Rating.Database
{
    public class RatingEntity
    {
        public RatingEntity() { }
        public RatingEntity(long gameId, string userId)
        {
            GameId = gameId;
            UserId = userId;
        }
        public long GameId { get; set; }
        public string UserId { get; set; }
        public GameEntity Game { get; set; }
        public UserEntity User { get; set; }
        public ICollection<RatingCategoryEntity> RatingCategories { get; set; } = new List<RatingCategoryEntity>();
    }
}
