﻿using AutoMapper;
using game_platform.Rating.RatingCategories;
using game_platform.Rating.RatingStrategy;
using game_platform.Rating.RatingStrategy.RatingStrategyFactory;
using game_platform.Rating.Web.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace game_platform.Rating.Web
{
    [Route("api/[controller]")]
    public class RatingsController : Controller
    {
        private readonly IRatingService _ratingService;
        private readonly IRatingStrategyFactory _factory;
        private readonly IMapper _mapper;

        public RatingsController(IMapper mapper, IRatingService ratingService, IRatingStrategyFactory factory)
        {
            _mapper = mapper;
            _ratingService = ratingService;
            _factory = factory;
        }

        [HttpPost]
        public IActionResult AddRating([FromBody] AddRatingDTO ratingDTO)
        {
            Rating rating = new Rating(
                ratingDTO.GameId,
                ratingDTO.UserId,
                CommentOrNull(ratingDTO),
                RatingCategoriesIn(ratingDTO),
                _factory.GetStrategy()
            );

            var result = _mapper.Map<RatingDTO>(_ratingService.AddRating(rating));
            if (result != null)
            {
                return Created($"/api/ratings?gameId={result.GameId}&userId={result.UserId}", result);
            }
            // TODO: Differentiate  between Game did not exist and,
            // score was not in range
            return NotFound("This game does not exist.");
        }

        private ICollection<RatingCategory> RatingCategoriesIn(AddRatingDTO ratingDTO) =>
            new List<RatingCategory>
            {
                new RatingCategory { Name = RatingCategory.CategoryType.Graphics, Score = ratingDTO.Graphics },
                new RatingCategory { Name = RatingCategory.CategoryType.Sound, Score = ratingDTO.Sound },
                new RatingCategory { Name = RatingCategory.CategoryType.Addiction, Score = ratingDTO.Addiction },
                new RatingCategory { Name = RatingCategory.CategoryType.Action, Score = ratingDTO.Action }
            };

        private Comment.Comment CommentOrNull(AddRatingDTO ratingDTO) =>
            string.IsNullOrWhiteSpace(ratingDTO.Subject) && string.IsNullOrWhiteSpace(ratingDTO.CommentMessage)
                ? null
                : new Comment.Comment { Subject = ratingDTO.Subject, Message = ratingDTO.CommentMessage };

        [HttpGet]
        public IActionResult GetAllRatings()
        {
            IList<RatingDTO> ratingDTOs = _mapper.Map<IList<RatingDTO>>(_ratingService.GetAllRatings());
            return Ok(ratingDTOs);
        }

        [HttpGet]
        public IActionResult GetRatingById([FromQuery] long gameId, string userId)
        {
            RatingDTO ratingDTO = _mapper.Map<RatingDTO>(_ratingService.GetRatingById(gameId, userId));
            return Ok(ratingDTO);
        }
    }
}
