﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace game_platform.Rating.Web.Dto
{
    public class AddRatingDTO
    {
        public string Subject { get; set; }

        public string CommentMessage { get; set; }

        public int Graphics { get; set; }

        public int Sound { get; set; }

        public int Addiction { get; set; }

        public int Action { get; set; }

        public string UserId { get; set; }
        public long GameId { get; set; }

    }
}
