﻿namespace game_platform.Rating.Web.Dto
{
    public class RatingDTO
    {
        public long GameId { get; set; }
        public string UserId { get; set; }
    }
}
