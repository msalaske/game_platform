﻿using game_platform.Rating.Database;
using System;
using System.ComponentModel.DataAnnotations;

namespace game_platform.Rating.RatingCategories.Database
{
    public class RatingCategoryEntity : BaseEntity
    {
        public enum CategoryType
        {
            Graphics,
            Sound,
            Addiction,
            Action
        }
        public CategoryType Name { get; set; }

        [Range(1, 5)]
        public int Score { get; set; }
        public RatingEntity Rating { get; set; }
    }
}
