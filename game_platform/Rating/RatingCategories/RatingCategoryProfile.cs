﻿using AutoMapper;
using game_platform.Rating.RatingCategories.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingCategories
{
    public class RatingCategoryProfile : Profile
    {
        public RatingCategoryProfile()
        {
            CreateMap<RatingCategoryEntity, RatingCategory>().ForMember(x => x.Rating, opt => opt.MapFrom(y => y.Rating)).ReverseMap();
        }
    }
}
