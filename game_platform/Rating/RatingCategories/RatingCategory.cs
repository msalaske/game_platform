﻿namespace game_platform.Rating.RatingCategories
{
    public class RatingCategory : BaseModel
    {
        public enum CategoryType
        {
            Graphics,
            Sound,
            Addiction,
            Action
        }
        public CategoryType Name { get; set; }
        public int Score { get; set; }
        public Rating Rating { get; set; }
    }
}
