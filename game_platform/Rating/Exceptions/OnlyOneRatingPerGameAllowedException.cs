﻿using System;

namespace game_platform.Rating.Exceptions
{
    public class OnlyOneRatingPerGameAllowedException : Exception
    {
        public OnlyOneRatingPerGameAllowedException() { }
        public OnlyOneRatingPerGameAllowedException(string message) : base(message) { }
        public OnlyOneRatingPerGameAllowedException(long GameId, string UserId) : base($"For user with id {UserId} already exists a rating for game {GameId}") { }
        public OnlyOneRatingPerGameAllowedException(string message, Exception inner) : base(message, inner) { }
    }
}