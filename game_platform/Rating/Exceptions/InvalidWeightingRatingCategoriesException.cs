﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.Exceptions
{
    public class InvalidWeightingRatingCategoriesException : Exception
    {
        public InvalidWeightingRatingCategoriesException() { }
        public InvalidWeightingRatingCategoriesException(string message) : base(message) { }
        public InvalidWeightingRatingCategoriesException(double weightAddiction, double weightSound, double weightAction, double weightGraphics) : base($"Invalid weighting vector (Action: {weightAction}, Graphics: {weightGraphics}, Sound: {weightSound}, Addiction: {weightAddiction}.") { }
        public InvalidWeightingRatingCategoriesException(string message, Exception inner) : base(message, inner) { }
    }
}
