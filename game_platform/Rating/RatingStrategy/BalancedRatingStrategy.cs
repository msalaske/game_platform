﻿using game_platform.Rating.RatingCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingStrategy
{
    public class BalancedRatingStrategy : IRatingStrategy
    {
        public double GetScore(ICollection<RatingCategory> ratingCategories)
        {
            return ratingCategories.Average(it => it.Score);
        }
    }
}
