﻿using game_platform.Rating.RatingCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingStrategy
{
    public interface IRatingStrategy
    {

        public double GetScore(ICollection<RatingCategory> ratingCategories);
    }
}
