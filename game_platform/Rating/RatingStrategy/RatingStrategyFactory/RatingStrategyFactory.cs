﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingStrategy.RatingStrategyFactory
{
    public class RatingStrategyFactory : IRatingStrategyFactory
    {
        private readonly RatingStrategyConfig _ratingStrategyConfig;

        public RatingStrategyFactory(IOptions<RatingStrategyConfig> ratingStrategyConfig)
        {
            _ratingStrategyConfig = ratingStrategyConfig.Value;
        }

        public IRatingStrategy GetStrategy()
        {
            if(_ratingStrategyConfig.RatingStrategyName == "BalancedRatingStrategy")
            {
                return new BalancedRatingStrategy();
            }
            return new CustomizedWeightedRatingStrategy(
                                                        weightAddiction: _ratingStrategyConfig.RatingCategoryWeights["Addiction"],
                                                        weightGraphics: _ratingStrategyConfig.RatingCategoryWeights["Graphics"], 
                                                        weightSound: _ratingStrategyConfig.RatingCategoryWeights["Sound"], 
                                                        weightAction: _ratingStrategyConfig.RatingCategoryWeights["Action"]);
        }
    }
}
