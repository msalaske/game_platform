﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingStrategy.RatingStrategyFactory
{
    public class RatingStrategyConfig
    {
        public string RatingStrategyName { get; set; }
        public Dictionary<string,double> RatingCategoryWeights { get; set; }
    }
}
