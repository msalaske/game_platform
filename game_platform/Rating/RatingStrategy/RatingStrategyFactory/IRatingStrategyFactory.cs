﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingStrategy.RatingStrategyFactory
{
    public interface IRatingStrategyFactory
    {
        public IRatingStrategy GetStrategy();
    }
}
