﻿using game_platform.Rating.Exceptions;
using game_platform.Rating.RatingCategories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating.RatingStrategy
{
    public class CustomizedWeightedRatingStrategy : IRatingStrategy
    {

        public double WeightAddiction {get; set;}
        public double WeightGraphics { get; set; }
        public double WeightSound { get; set; }
        public double WeightAction { get; set; }

        public CustomizedWeightedRatingStrategy(double weightAddiction, double weightGraphics, double weightSound, double weightAction)
        {
            this.WeightAddiction = weightAddiction;
            this.WeightGraphics = weightGraphics;
            this.WeightSound = weightSound;
            this.WeightAction = weightAction;
        }

        public double GetScore(ICollection<RatingCategory> ratingCategories)
        {
            if((WeightAction + WeightGraphics + WeightSound + WeightAddiction) != 1)
            {
                throw new InvalidWeightingRatingCategoriesException(WeightAddiction, WeightSound, WeightAction, WeightGraphics);
            }
            var scoreGraphics = ScoreOf(ratingCategories, RatingCategory.CategoryType.Graphics);
            var scoreAddiction = ScoreOf(ratingCategories, RatingCategory.CategoryType.Addiction);
            var scoreSound = ScoreOf(ratingCategories, RatingCategory.CategoryType.Sound);
            var scoreAction = ScoreOf(ratingCategories, RatingCategory.CategoryType.Action);

            return (WeightAddiction * scoreAddiction) + (WeightGraphics * scoreGraphics) + (WeightSound * scoreSound) + (WeightAction * scoreAction);
        }

        private double ScoreOf(ICollection<RatingCategory> ratingCategories, RatingCategory.CategoryType type)
        {
            return (ratingCategories.Where(x => x.Name == type).Select(x => x.Score).SingleOrDefault() != 0) ? ratingCategories.Where(x => x.Name == type).Select(x => x.Score).SingleOrDefault() : 0;
        }
    }
}
