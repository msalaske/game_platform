﻿using game_platform.Rating.Database;
using System;

namespace game_platform.Rating.Comment.Database
{
    public class CommentEntity : BaseEntity
    {
        public string Subject { get; set; }
        public string Message { get; set; }
        public long RatingGameId { get; set; }
        public string RatingUserId { get; set; }
        public RatingEntity Rating { get; set; }
    }
}
