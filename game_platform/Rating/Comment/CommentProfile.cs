﻿using AutoMapper;
using game_platform.Rating.Comment.Database;

namespace game_platform.Rating.Comment
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMap<Comment, CommentEntity>().ReverseMap();
        }
    }
}
