﻿using System;

namespace game_platform.Rating.Comment
{
    public class Comment : BaseModel
    {
        public long RatingId { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

    }
}
