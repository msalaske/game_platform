﻿using AutoMapper;
using game_platform.Rating.Database;
using game_platform.Rating.Web.Dto;

namespace game_platform.Rating
{
    public class RatingProfile : Profile
    {
        public RatingProfile()
        {
            CreateMap<Rating, RatingDTO>().ReverseMap();
            CreateMap<RatingEntity, Rating>().ForMember(x => x.GameId, opt => opt.MapFrom(y => y.Game.Id)).ForMember(x => x.RatingCategories, opt => opt.MapFrom(x => x.RatingCategories)).ReverseMap();
        }
    }
}
