﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Rating
{
    public interface IRatingService
    {
        Rating AddRating(Rating rating);
        IList<Rating> GetAllRatings();
        Rating GetRatingById(long gameId, string userId);
    }
}
