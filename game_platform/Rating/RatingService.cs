﻿using System.Collections.Generic;
using System.Linq;
using game_platform.Game;
using game_platform.Game.Exceptions;
using game_platform.Rating.Database;
using game_platform.Game.Subscription;

namespace game_platform.Rating
{
    public class RatingService : IRatingService
    {
        private readonly IRatingRepository _ratingRepository;
        private readonly IGameService _gameService;
        private readonly ISubscriptionService _subscriptionService;
        public RatingService(IGameService gameService, IRatingRepository ratingRepository, ISubscriptionService subscriptionService)
        {
            _ratingRepository = ratingRepository;
            _subscriptionService = subscriptionService;
            _gameService = gameService;
        }


        public Rating AddRating(Rating rating)
        {
            if (rating.RatingCategories.Any(x => x.Score < 1 || x.Score > 5)) throw new NotValidScoreRangeException();
            Rating result = _ratingRepository.Insert(rating);
            
            // if new rating added, notify observers.
            ObservableGame observableGame = _subscriptionService.GetObservable(result.GameId);
            observableGame = _subscriptionService.SetObserversAndReturnObservable(observableGame);
            observableGame.Notify();   
            return result;
        }
        public IList<Rating> GetAllRatings()
        {

            return _ratingRepository.GetAll();
        }

        public Rating GetRatingById(long GameId, string UserId)
        {
            return _ratingRepository.GetById(GameId, UserId);
        }

    }
}
