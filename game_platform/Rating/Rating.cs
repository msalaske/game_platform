﻿using game_platform.Rating.RatingCategories;
using game_platform.Rating.RatingStrategy;
using System.Collections.Generic;
using System.Linq;

namespace game_platform.Rating
{
    public class Rating
    {
        private readonly IRatingStrategy _ratingStrategy;

        public Rating(long gameId) { }

        public Rating(long gameId, string userId, Comment.Comment comment, ICollection<RatingCategory> ratingCategories, IRatingStrategy _ratingStrategy)
        {
            GameId = gameId;
            UserId = userId;
            Comment = comment;
            RatingCategories = ratingCategories;
            this._ratingStrategy = _ratingStrategy;
            Score = this._ratingStrategy.GetScore(RatingCategories);
        }

        public long GameId { get; set; }

        public string UserId { get; set; }

        public double Score { get; set; }

        public long? CommentId => Comment?.Id;

        public Comment.Comment Comment { get; set; }

        public ICollection<RatingCategory> RatingCategories { get; set; } = new List<RatingCategory>();
    }
}
