using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Text.Json.Serialization;
using AutoMapper;
using game_platform.Game;
using game_platform.Game.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection.Extensions;
using game_platform.Database;
using game_platform.Rating.Database;
using game_platform.Rating;
using game_platform.User.Database;
using game_platform.User;
using game_platform.Logging;
using game_platform.Rating.RatingStrategy.RatingStrategyFactory;
using game_platform.Game.Subscription.Database;
using game_platform.Game.Subscription;

namespace game_platform
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<DatabaseContext>();

            services.AddSingleton<ILog, ConsoleLog>();

            services.AddTransient<IGameService, GameService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IAuthRepository, AuthRepository>();
            services.AddTransient<IRatingService, RatingService>();
            services.AddTransient<IGameRepository, GameRepository>();
            services.AddTransient<IRatingRepository, RatingRepository>();
            services.AddTransient<IPlatformRepository, PlatformRepository>();
            services.AddTransient<IPlatformService, PlatformService>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IRatingStrategyFactory, RatingStrategyFactory>();
            services.AddTransient<ISubscriptionService, SubscriptionService>();
            services.AddTransient<ISubscriptionRepository, SubscriptionRepository>();
            services.AddOptions();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<RatingStrategyConfig>(Configuration.GetSection("RatingStrategyConfig"));

            services.AddCors();
            services.AddAuthentication();
            services.AddIdentity<UserEntity, IdentityRole>(opt =>
            {
                /*
                // set password policy requirements, define this!
                opt.Password.RequiredLength = 7;
                opt.Password.RequireDigit = false;
                opt.Password.RequireUppercase = false;
                */
                opt.User.RequireUniqueEmail = true;
                opt.SignIn.RequireConfirmedEmail = true;
            }).AddEntityFrameworkStores<DatabaseContext>().AddDefaultTokenProviders();

            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));


                // In production, the React files will be served from this directory
                services.AddSpaStaticFiles(configuration =>
            {
                
                configuration.RootPath = "ClientApp/build";

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)


        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => builder
                   .AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader());
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseAuthentication();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
