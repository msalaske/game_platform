﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Logging
{
    public class ConsoleLog : ILog
    {
        public void Info(string str)
        {
            Console.WriteLine(str);
        }
    }
}
