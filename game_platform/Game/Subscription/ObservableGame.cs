﻿using System.Collections.Generic;

namespace game_platform.Game.Subscription
{
    public class ObservableGame : IObservable
    {
        public ObservableGame() { }
        public ObservableGame(Game game)
        {
            Game = game;
        }

        public Game Game { get; set; }
        public ICollection<IObserver> Observers { get; set; } = new List<IObserver>();

        public void Register(IObserver observer)
        {
            if (!Observers.Contains(observer))
            {
                Observers.Add(observer);
            }
        }

        public void Unregister(IObserver observer)
        {
            if (Observers.Contains(observer))
            {
                Observers.Remove(observer);
            }
        }

        public void Notify()
        {
            foreach (IObserver observer in Observers)
            {
                observer.Update();
            }
        }

        public Game GetGame()
        {
            return Game;
        }
    }
}