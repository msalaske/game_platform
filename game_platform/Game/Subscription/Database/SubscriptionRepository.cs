﻿using AutoMapper;
using game_platform.Database;
using game_platform.ExceptionHandling;
using game_platform.Game.Database;
using game_platform.User.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace game_platform.Game.Subscription.Database
{
    public class SubscriptionRepository : ISubscriptionRepository
    {
        private readonly DatabaseContext context;
        private readonly IMapper _mapper;

        public SubscriptionRepository(DatabaseContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }

        public ObservableGame Add(ObservableGame updatedObservableGame, SubscribingUser subscribingUser)
        {
            GameEntity observableGameEntity = context.Games.Where(x => x.Id == updatedObservableGame.Game.Id).SingleOrDefault();
            observableGameEntity.Subscriptions.Add(new SubscriptionEntity()
            {
                ObservableGame = observableGameEntity,
                SubscribingUser = context.Users.Find(subscribingUser.UserId)
            });
            context.Entry(observableGameEntity).State = EntityState.Modified;
            context.SaveChangesAsync();
            return updatedObservableGame;
        }

        public ObservableGame Remove(ObservableGame updatedObservableGame, SubscribingUser subscribingUser)
        {
            GameEntity observableGameEntity = context.Games.Where(x => x.Id == updatedObservableGame.Game.Id).SingleOrDefault();
            var ToBeRemovedSubscription = context.Subscriptions.Where(x => (x.UserId == subscribingUser.UserId) && (x.ObservableGame.Id == observableGameEntity.Id)).Include(x => x.ObservableGame).Include(x => x.SubscribingUser).SingleOrDefault();
            observableGameEntity.Subscriptions.Remove(ToBeRemovedSubscription);
            context.Entry(observableGameEntity).State = EntityState.Modified;
            context.SaveChangesAsync();
            return updatedObservableGame;
        }


        public ObservableGame GetObservableGame(long gameId)
        {
            GameEntity game = context.Games.Where(x => x.Id == gameId).Include(x => x.Ratings).Include(x => x.Subscriptions).SingleOrDefault();
            ObservableGame observableGame = new ObservableGame(_mapper.Map<Game>(game));
            return observableGame;
        }

        public IList<SubscribingUserModel> GetObservers(long gameId)
        {
            GameEntity observableGameEntity = context.Games.Where(x => x.Id == gameId).Include(x => x.Subscriptions).ThenInclude(x => x.SubscribingUser).SingleOrDefault();
            IList<SubscribingUserModel> observers = new List<SubscribingUserModel>();
            foreach (var subscription in observableGameEntity.Subscriptions)
            {
                observers.Add(_mapper.Map<SubscribingUserModel>(subscription.SubscribingUser));
            }
            return observers;
        }
    }
}
