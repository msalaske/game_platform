﻿using game_platform.User.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription.Database
{
    public interface ISubscriptionRepository
    {
        public ObservableGame GetObservableGame(long gameId);
        public ObservableGame Add(ObservableGame updatedObservableGame, SubscribingUser subscribingUser);
        public ObservableGame Remove(ObservableGame updatedObservableGame, SubscribingUser subscribingUser);
        public IList<SubscribingUserModel> GetObservers(long gameId);
    }
}
