﻿using game_platform.Game.Database;
using game_platform.User.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription.Database
{
    public class SubscriptionEntity : BaseEntity
    {
        public long GameId { get; set; }
        public GameEntity ObservableGame { get; set; }
        public string UserId { get; set; }
        public UserEntity SubscribingUser { get; set; }
    }
}
