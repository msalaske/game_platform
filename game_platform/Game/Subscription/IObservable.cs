﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription
{
    public interface IObservable
    {
        public void Register(IObserver o);
        public void Unregister(IObserver o);
        public void Notify();
    }
}
