﻿using AutoMapper;
using game_platform.Game.Database;
using game_platform.Game.Subscription.Database;
using game_platform.User.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription
{
    public class SubscriptionProfile : Profile
    {

        public SubscriptionProfile()
        {
            CreateMap<GameEntity, ObservableGame>().ForMember(x => x.Observers, opt => opt.MapFrom(y => y.Subscriptions.Select(x => x.SubscribingUser).ToList())).ReverseMap();
            CreateMap<UserEntity, SubscribingUserModel>().ForMember(x => x.UserId, opt => opt.MapFrom(y => y.Id)).ReverseMap();
            CreateMap<UserEntity, SubscribingUser>().ForMember(x => x.ObservableGame, opt => opt.MapFrom(y => y.Subscriptions.Select(x => x.ObservableGame))).ReverseMap();
        }
    }
}
