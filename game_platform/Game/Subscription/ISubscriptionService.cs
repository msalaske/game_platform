﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription
{
    public interface ISubscriptionService
    {
        public void Subscribe(long gameId, string userId);
        public void UnSubscribe(long gameId, string userId);
        public ObservableGame GetObservable(long gameId);
        public ObservableGame SetObserversAndReturnObservable(ObservableGame observableGame);
    }
}
