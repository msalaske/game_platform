﻿using game_platform.User;

namespace game_platform.Game.Subscription
{
    public class SubscribingUser : IObserver
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public ObservableGame ObservableGame { get; set; }

        private readonly IEmailSender _emailSender;

        public SubscribingUser(string userId, ObservableGame observableGame, string email, IEmailSender emailSender)
        {
            UserId = userId;
            ObservableGame = observableGame;
            Email = email;
            _emailSender = emailSender;
        }
        public void Update()
        {
            
            _emailSender.SendEmailAsync(Email, $"A game has changed which you subscribed!", $"Game {ObservableGame.GetGame().Title} with Id {ObservableGame.GetGame().Id} has changed!");
        }
    }
}
