﻿using game_platform.Game.Subscription.Database;
using game_platform.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IAuthService _authService;
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IEmailSender _emailSender;

        public SubscriptionService(IAuthService authService, ISubscriptionRepository subscriptionRespository, IEmailSender emailSender)
        {
            _authService = authService;
            _subscriptionRepository = subscriptionRespository;
            _emailSender = emailSender;
        }

        public async void Subscribe(long gameId, string userId)
        {
            ObservableGame observableGame = _subscriptionRepository.GetObservableGame(gameId);
            // set observers
            observableGame = SetObserversAndReturnObservable(observableGame);
            User.User user = await _authService.GetUserById(userId);
            SubscribingUser subscribingUser = new SubscribingUser(user.UserId, observableGame, user.Email, _emailSender);
            if (!ExistsAlready(observableGame.Observers, subscribingUser))
            {
                observableGame.Register(subscribingUser);
                _subscriptionRepository.Add(observableGame, subscribingUser);
            }
        }

        private bool ExistsAlready(ICollection<IObserver> observer, SubscribingUser user)
        {
            foreach (SubscribingUser u in observer)
            {
                if (u.UserId == user.UserId)
                {
                    return true;
                }
            }
            return false;
        }
        public async void UnSubscribe(long gameId, string userId)
        {
            ObservableGame observableGame = _subscriptionRepository.GetObservableGame(gameId);
            // set observers
            observableGame = SetObserversAndReturnObservable(observableGame);
            User.User user = await _authService.GetUserById(userId);
            SubscribingUser subscribingUser = new SubscribingUser(user.UserId, observableGame, user.Email, _emailSender);
            if (ExistsAlready(observableGame.Observers, subscribingUser))
            {
                observableGame.Unregister(subscribingUser);
                _subscriptionRepository.Remove(observableGame, subscribingUser);
            }
        }

        public ObservableGame GetObservable(long gameId)
        {
            return _subscriptionRepository.GetObservableGame(gameId);
        }

        public ObservableGame SetObserversAndReturnObservable(ObservableGame observableGame)
        {
            IList<SubscribingUserModel> subscribingUsers = _subscriptionRepository.GetObservers(observableGame.Game.Id);
            foreach (var observer in subscribingUsers)
            {
                observableGame.Register(new SubscribingUser(observer.UserId, observableGame, observer.Email, _emailSender));
            }
            return observableGame;
        }
    }
}
