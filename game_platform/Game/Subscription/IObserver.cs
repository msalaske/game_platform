﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription
{
    public interface IObserver
    {
        public void Update();
    }
}
