﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Web.Dto
{
    public class SubscribeGameDTO
    {
        public string UserId { get; set; }
        public long GameId { get; set; }
    }
}
