﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game_platform.Game.Subscription
{
    public class SubscribingUserModel
    {
        public string Email { get; set; }
        public string UserId { get; set; }
    }
}
