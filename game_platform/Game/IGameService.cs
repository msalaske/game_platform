﻿using System.Collections.Generic;

namespace game_platform.Game
{
    public interface IGameService
    {
        IList<Game> GetNewestGames(long platformId);

        IList<Game> GetMostPopularGames(long platformId);
       
        Game GetDetailedInformation(long GameId);

        IList<Platform> GetPlatformsByGameId(long GameId);

        Game AddGame(Game game);
        
        IList<Game> GetAllGames();
        Game GetGameById(long GameId);
        Game DeleteGameById(long gameId);

        IList<Game> SearchbyString(string Title);
    }
}
