﻿using System.Linq;
using AutoMapper;
using game_platform.Game.Database;
using game_platform.Game.Web.Dto;

namespace game_platform.Game
{
    public class PlatformProfile : Profile
    {

        public PlatformProfile()
        {
            CreateMap<PlatformDTO, Platform>().ReverseMap();
            CreateMap<PlatformEntity, Platform>().ForMember(platform => platform.Games, opt => opt.MapFrom(x => x.PlatformGames.Select(y => y.Game).ToList())).ReverseMap();
            CreateMap<PlatformWithGamesDTO, Platform>().ForMember(x => x.Games, opt => opt.MapFrom(y => y.Games)).ReverseMap();
        }
    }
}
