﻿namespace game_platform.Game.Web.Dto
{
    public class PlatformDTO

    {

        public enum PlatformType
        {


            XBOX,
            Playstation,
            Nintendo,
            PC
        }
        public long Id { get; set; }
        public PlatformDTO.PlatformType Name { get; set; }

    }
}
