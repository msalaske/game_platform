﻿using System.Collections.Generic;

namespace game_platform.Game.Web.Dto
{
    public class PlatformWithGamesDTO
    {

        public enum PlatformType
        {


            XBOX,
            Playstation,
            Nintento,
            PC
        }
        public long Id { get; set; }
        public PlatformDTO.PlatformType Name { get; set; }

        public List<BasicGameDTO> Games { get; set; }

    }
}
