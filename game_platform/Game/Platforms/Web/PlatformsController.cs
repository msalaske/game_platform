﻿using System.Collections.Generic;
using AutoMapper;
using game_platform.Game.Web.Dto;
using Microsoft.AspNetCore.Mvc;

namespace game_platform.Game.Web
{
    [Route("api/[controller]")]
    public class PlatformsController : Controller
    {
        private readonly IPlatformService _platformService;
        private readonly IMapper _mapper;

        public PlatformsController(IPlatformService platformService, IMapper mapper)
        {
            _platformService = platformService;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult AddPlatform([FromBody] PlatformDTO platformDTO)
        {
            Platform platform = _mapper.Map<Platform>(platformDTO);

            var result = _mapper.Map<PlatformDTO>(this._platformService.AddPlatform(platform));
            return Created("/api/platforms/" + result.Id, result);
        }

        [HttpGet]
        public IActionResult Platforms()
        {

            var result = _mapper.Map<IList<PlatformDTO>>(this._platformService.GetAllPlatforms());
            return Ok(result);
        }

        [HttpGet]
        [Route("{platformId:long}")]
        public IActionResult GetPlatformById(long platformId)
        {

            var result = _mapper.Map<PlatformDTO>(this._platformService.GetPlatformById(platformId));
            return Ok(result);
        }

        [HttpGet]
        [Route("{platformId:long}/games")]
        public IActionResult GetAllGamesOfAPlatform(long platformId)
        {
            var result = _mapper.Map<PlatformWithGamesDTO>(this._platformService.GetAllGamesAssociatedWithPlatform(platformId));
            return Ok(result);
        }
    }
}
