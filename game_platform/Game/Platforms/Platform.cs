﻿using System.Collections.Generic;

namespace game_platform.Game
{
    public class Platform : BaseModel
    {

        public enum PlatformType
        {


            XBOX = 1,
            Playstation = 2,
            Nintendo = 3,
            PC = 4
        }

        public Platform()
        {
            Games = new List<Game>();
        }

        public PlatformType Name { get; set; }

        public ICollection<Game> Games { get; set; }
        
    }
}
