﻿using System.Collections.Generic;

namespace game_platform.Game
{
    public interface IPlatformService
    {
        Platform AddPlatform(Platform platform);
        IList<Platform> GetAllPlatforms();
        Platform GetPlatformById(long platformId);
        Platform GetAllGamesAssociatedWithPlatform(long platformId);
    }
}
