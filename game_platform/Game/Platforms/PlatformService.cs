﻿using System.Collections.Generic;
using game_platform.Game.Platforms.Database;

namespace game_platform.Game.Platforms
{
    public class PlatformService : IPlatformService
    {
        private readonly IPlatformRepository _platformRepository;

        public PlatformService(IPlatformRepository platformRepository)
        {
            _platformRepository = platformRepository;
        }

        public Platform AddPlatform(Platform platform)
        {
            return this._platformRepository.Insert(platform);
        }

        public IList<Platform> GetAllPlatforms()
        {     
            return this._platformRepository.GetAll();
        }

        public Platform GetPlatformById(long platformId)
        {
             
            return this._platformRepository.GetById(platformId);
        }

        public Platform GetAllGamesAssociatedWithPlatform(long platformId)
        {
            
            return this._platformRepository.GetById(platformId);
        }

    }
}
