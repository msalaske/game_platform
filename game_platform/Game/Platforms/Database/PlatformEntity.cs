﻿using System.Collections.Generic;

namespace game_platform.Game.Database
{
    public class PlatformEntity : BaseEntity
    {
        public enum PlatformType
        {
            XBOX,
            Playstation,
            Nintendo,
            PC
        }
        public PlatformEntity(){
            this.PlatformGames = new List<PlatformGameEntity>();
        }
        public PlatformType Name { get; set; }
        public ICollection<PlatformGameEntity> PlatformGames { get; set; }
    }
}
