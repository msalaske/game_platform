﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using game_platform.Database;
using Microsoft.EntityFrameworkCore;

namespace game_platform.Game.Platforms.Database
{
    public class PlatformRepository :  IPlatformRepository
    {

        private readonly IMapper _mapper;
        private readonly DatabaseContext context;

        public PlatformRepository(DatabaseContext context, IMapper mapper) 
        {
            this.context = context;
            _mapper = mapper;
        }

        public Platform Delete(long id)
        {
            if (id == 0) throw new ApplicationException("Missing platform id");

            var entity = context.Platforms.SingleOrDefault(s => s.Id == id);
            context.Platforms.Remove(entity);
            context.SaveChanges();
            return _mapper.Map<Platform>(entity);
        }

        public  Platform GetById(long platformId)
        {
            Platform platform = _mapper.Map<Platform>(context.Platforms.Where(x => x.Id == platformId).Include(x => x.PlatformGames).ThenInclude(x => x.Game).SingleOrDefault());
            return platform;
        }

        public Platform Insert(Platform platform)
        {
            var entity = _mapper.Map<PlatformEntity>(platform);
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            else
            {
                context.Platforms.Add(entity);
                context.SaveChanges();
                return _mapper.Map<Platform>(entity);
            }
        }

        public IList<Platform> GetAll()
        {
            return _mapper.Map<IList<Platform>>(context.Platforms.ToList());
        }

        public Platform Update(Platform platform)
        {
            PlatformEntity entity = _mapper.Map<PlatformEntity>(platform);
            if (entity == null) throw new ArgumentNullException("entity");
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
            return _mapper.Map<Platform>(entity);
        }
    }
}
