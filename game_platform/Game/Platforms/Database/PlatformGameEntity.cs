﻿namespace game_platform.Game.Database
{
    public class PlatformGameEntity : BaseEntity
    {
        public GameEntity Game { get; set; }

        public PlatformEntity Platform { get; set; }
    }
}
