﻿using System.Collections.Generic;

namespace game_platform.Game.Platforms.Database
{
    public interface IPlatformRepository 
    {
        Platform Delete(long id);
        Platform Insert(Platform platform);
        Platform Update(Platform platform);
        Platform GetById(long platformId);
        IList<Platform> GetAll();

    }
}
