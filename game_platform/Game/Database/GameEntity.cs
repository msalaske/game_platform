﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using game_platform.Game.Subscription.Database;
using game_platform.Rating.Database;

namespace game_platform.Game.Database
{
    public class GameEntity : BaseEntity
    {
        public GameEntity()
        {
            this.Ratings = new List<RatingEntity>();
            this.PlatformGames = new List<PlatformGameEntity>();
            this.Subscriptions = new List<SubscriptionEntity>();
        }
        [Required]
        public string Title { get; set; }
        [Required]
        public DateTime PublicationDate { get; set; }
        [Required]
        public string Producer { get; set; }  
        public string YouTubeTrailer { get; set; }
        [Required]
        public string Description { get; set; }
        public ICollection<RatingEntity> Ratings { get; set; }
        public ICollection<PlatformGameEntity> PlatformGames { get; set; } 

        public ICollection<SubscriptionEntity> Subscriptions { get; set; }
    }
}
