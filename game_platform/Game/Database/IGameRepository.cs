﻿using System.Collections.Generic;

namespace game_platform.Game.Database
{
    public interface IGameRepository
    {

        Game AssociatePlatformWithGame(GameEntity entity, long platformId);

        Game GetPlatformsByGameId(long gameId);

        IList<Game> GetGamesByPlatform(long platformId);

        IList<Game> Search(string title);

        Game GetById(long id);

        IList<Game> GetAll();

        Game Delete(long id);

        Game Insert(Game game);

        Game Update(Game game);
    }
}
