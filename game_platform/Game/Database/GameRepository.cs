﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using game_platform.Database;
using game_platform.ExceptionHandling;
using game_platform.Rating.RatingStrategy.RatingStrategyFactory;
using Microsoft.EntityFrameworkCore;

namespace game_platform.Game.Database
{
    public class GameRepository : IGameRepository
    {

        private readonly IMapper _mapper;
        private readonly IRatingStrategyFactory _factory;
        private readonly DatabaseContext context;

        public GameRepository(DatabaseContext context, IMapper mapper, IRatingStrategyFactory factory)
        {
            this.context = context;
            _mapper = mapper;
            _factory = factory;
        }

        public Game GetById(long gameId)
        {
            var result = _mapper.Map<Game>(context.Games.Where(x => x.Id == gameId).Include(x => x.Ratings).ThenInclude(x => x.RatingCategories).Include(x => x.PlatformGames).ThenInclude(x => x.Platform).SingleOrDefault());
            Game game = AssignScoreToRatingAndReturnGame(result);
            return game;

        }

        public Game GetPlatformsByGameId(long gameId)
        {
            var result = context.Games.Where(g => g.Id == gameId)
                .Include(g => g.PlatformGames)
                .ToList();

            return _mapper.Map<Game>(result);
        }

        public IList<Game> GetGamesByPlatform(long platformId)
        {
            var games  = context.Games
                .Include(g => g.PlatformGames)
                .Where(g => g.PlatformGames.Select(pg => pg.Platform.Id).Contains(platformId))
                .ToList();
            IList<Game> mappedGames = _mapper.Map<IList<Game>>(games);
            IList<Game> result = new List<Game>();
            foreach (var game in mappedGames)
            {
                result.Add(AssignScoreToRatingAndReturnGame(game));
            }
            return result;
        }

        private Game AssignScoreToRatingAndReturnGame(Game game)
        {
                IList<Rating.Rating> ratings = new List<Rating.Rating>();
                foreach (var rating in game.Ratings)
                {
                    Rating.Rating r = new Rating.Rating(game.Id, rating.UserId, rating.Comment, rating.RatingCategories, _factory.GetStrategy());
                    ratings.Add(r);
                }
                game.Ratings = ratings;
                return game;
        }
        public IList<Game> GetAll()
        {
            var games = context.Games.Include(g => g.Ratings).ThenInclude(g => g.RatingCategories).Include(p => p.PlatformGames).ThenInclude(x => x.Platform).ToList();
            IList<Game> mappedGames = _mapper.Map<IList<Game>>(games);
            IList<Game> result = new List<Game>();
            foreach(var game in mappedGames)
            {
                result.Add(AssignScoreToRatingAndReturnGame(game));
            }
            return result;
        }

        public Game GetByTitle(string title)
        {
            Game result = _mapper.Map<Game>(context.Games.Where(g => g.Title.Equals(title)).Include(g => g.Ratings).Include(p => p.PlatformGames).ThenInclude(x => x.Platform).SingleOrDefault());
            return result;
        }

        public Game AssociatePlatformWithGame(GameEntity entity, long platformId)
        {
            entity.PlatformGames.Add(new PlatformGameEntity
            {
                Game = entity,
                Platform = context.Platforms.Find(platformId)
            });
            context.SaveChanges();
            return _mapper.Map<Game>(entity);
        }


        public IList<Game> Search(string title)
        {
            IList<Game> games = _mapper.Map<IList<Game>>(context.Games.Where(x => x.Title.Contains(title)).Include(x => x.Ratings).Include(x => x.PlatformGames).ThenInclude(x => x.Platform).ToList());
            return games;
        }

        public Game Delete(long id)
        {
            if (id == 0) throw new ArgumentNullException("entity");
            GameEntity entity = context.Games.SingleOrDefault(s => s.Id == id);
            context.Games.Remove(entity);
            context.SaveChanges();
            return _mapper.Map<Game>(entity);
        }

        public Game Insert(Game game)
        {
            GameEntity entity = _mapper.Map<GameEntity>(game);
            // TODO: how to get annotations of an entity type, to make this more pretty.
            if ((entity.Title == null || entity.Producer == null || entity.Description == null || entity.PublicationDate == null))
            {
                throw new NotAllRequiredFieldsFilledOutException();
            }
            else
            {
                context.Games.Add(entity);
                context.SaveChanges();
                foreach (var platform in game.Platforms)
                {
                    AssociatePlatformWithGame(entity, platform.Id);
                }
                return _mapper.Map<Game>(entity);
            }
        }

        public Game Update(Game game)
        {
            GameEntity entity = _mapper.Map<GameEntity>(game);
            // TODO: how to get annotations of an entity type, to make this more pretty.
            if ((entity.Title == null || entity.Producer == null || entity.Description == null || entity.PublicationDate == null))
            {
                throw new NotAllRequiredFieldsFilledOutException();
            }
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
            return _mapper.Map<Game>(entity);
        }
    }
}
