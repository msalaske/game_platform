﻿using System.Linq;
using AutoMapper;
using game_platform.Game.Database;
using game_platform.Game.Web.Dto;

namespace game_platform.Game
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            CreateMap<Game, AddGameDTO>().ForMember(x => x.PlatformIds, opt => opt.MapFrom(x => x.Platforms.Select(y => y.Id).ToList())).ReverseMap();
            CreateMap<GameEntity, Game>().ForMember(game => game.Platforms, opt => opt
                                         .MapFrom(x => x.PlatformGames
                                         .Select(y => y.Platform)
                                         .ToList()))
                                         .ReverseMap();

            CreateMap<Game, GameDTO>().ForMember(x => x.Platforms, opt => opt.MapFrom(y => y.Platforms)).ReverseMap();
            CreateMap<Game, BasicGameDTO>().ReverseMap();
            CreateMap<Game, NewestGamesDTO>().ReverseMap();

            CreateMap<Game, DetailedGameDTO>().ForMember(x => x.PublicationYear, opt => opt.MapFrom(y => y.PublicationDate.Year)).ReverseMap();
            CreateMap<Game, PopularGameDTO>().ReverseMap();

            CreateMap<Game, DeleteGameDTO>().ReverseMap();
        }
    }
}
