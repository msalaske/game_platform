﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using game_platform.ExceptionHandling;
using game_platform.Game.Database;
using game_platform.Game.Exceptions;
using game_platform.User;
using game_platform.User.Database;

namespace game_platform.Game
{
    public class GameService : IGameService
    {

        private readonly IGameRepository _gameRepository;
        public GameService(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        public Game AddGame(Game game)
        {
            if (game == null)
            {
                throw new ArgumentNullException();
            }
            Game g = _gameRepository.Insert(game);
            return g;
        }

        public Game UpdateGame(Game game)
        {
            if (game == null)
            {
                throw new ArgumentNullException();
            }

            if (this.GetAllGames().Where(x => x.Title == game.Title).Any())
            {
                throw new MultipleGamesWithSameTitleNotAllowedExcpetion();
            }
            game = _gameRepository.Update(game);
            return game;
        }

        public IList<Platform> GetPlatformsByGameId(long GameId)
        {
            var result = this._gameRepository.GetPlatformsByGameId(GameId).Platforms.ToList();
            return result;
        }

        public Game DeleteGameById(long gameId)
        {
            if (this._gameRepository.GetById(gameId) == null)
            {
                throw new EntityDoesNotExistException();
            }
            return this._gameRepository.Delete(gameId);
        }

        public IList<Game> GetAllGames()
        {
            return this._gameRepository.GetAll();
        }

        private Game AssignAverageToGame(Game game)
        {
            IList<decimal> sequence = game.Ratings.Select(x => (decimal)x.Score).ToList();
            game.AverageScore = sequence.IsNullOrEmpty() ? 0 : sequence.Average();
            return game;
        }

        public Game GetDetailedInformation(long GameId)
        {
            Game game = this.GetGameById(GameId);
            Game result = AssignAverageToGame(game);
            return result;
        }

        public IList<Game> GetMostPopularGames(long platformId)
        {
            var games = GamesByPlatformOrAllGames(platformId);
            IList<Game> gamesWithScores = new List<Game>();
            foreach(var game in games)
            {
                Game g = AssignAverageToGame(game);
                gamesWithScores.Add(g);
            }
            var results = gamesWithScores
                .OrderByDescending(x => x.AverageScore)
                .ThenByDescending(x => x.PublicationDate).ToList();
            return results;
        }

        public IList<Game> GetNewestGames(long platformId)
        {
            var games = GamesByPlatformOrAllGames(platformId);
            
            IList<Game> result = games
                .OrderByDescending(x => x.PublicationDate)
                .ToList();

            return result;
        }

        private IList<Game> GamesByPlatformOrAllGames(long platformId) =>
            platformId == 0
                ? _gameRepository.GetAll()
                : _gameRepository.GetGamesByPlatform(platformId).ToList();

        public Game GetGameById(long GameId)
        {
            return _gameRepository.GetById(GameId);
        }

        public IList<Game> SearchbyString(string Title)
        {
            return _gameRepository.Search(Title);
        }
    }
}
