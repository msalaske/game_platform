﻿using System;

namespace game_platform.Game.Exceptions
{
    public class NotValidScoreRangeException : Exception
    {
        public NotValidScoreRangeException() {}
        public NotValidScoreRangeException(string message): base(message) {}
        public NotValidScoreRangeException(string message, Exception inner): base(message, inner) {}
    }
}
