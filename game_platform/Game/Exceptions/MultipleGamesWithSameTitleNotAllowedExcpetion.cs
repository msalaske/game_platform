﻿using System;

namespace game_platform.Game.Exceptions
{
    public class MultipleGamesWithSameTitleNotAllowedExcpetion : Exception
    {
        public MultipleGamesWithSameTitleNotAllowedExcpetion(){}
        public MultipleGamesWithSameTitleNotAllowedExcpetion(string message): base(message) {}
        public MultipleGamesWithSameTitleNotAllowedExcpetion(string message, Exception inner): base(message, inner) {}
    }
}
