﻿using System.Collections.Generic;
using AutoMapper;
using game_platform.Game.Subscription;
using game_platform.Game.Web.Dto;
using game_platform.Logging;
using Microsoft.AspNetCore.Mvc;

namespace game_platform.Game.Web
{

    [Route("api/[controller]")]
    public class GamesController : Controller
    {

  
        private readonly IGameService _gameService;
        private readonly ILog _log;
        private readonly IMapper _mapper;
        private readonly ISubscriptionService _subscriptionService;

        public GamesController(IGameService gamePlatform, ISubscriptionService subscriptionService, IMapper mapper, ILog log)
        {
            _gameService = gamePlatform;
            _subscriptionService = subscriptionService;
            _mapper = mapper;
            _log = log;
        }

        [HttpPost]
        public IActionResult AddGame([FromBody] AddGameDTO gameDTO)
        {

            _log.Info("POST /api/games");
            Game game = _mapper.Map<Game>(gameDTO);
            foreach (var platformId in gameDTO.PlatformIds)
            {
                PlatformDTO platformDTO = new PlatformDTO { Id = platformId };
                Platform platform = _mapper.Map<Platform>(platformDTO);
                game.Platforms.Add(platform);
            }
            Game result = this._gameService.AddGame(game);
            return Created("/api/games/" + result.Id, _mapper.Map<AddGameDTO>(result));
        }


        [Route("{gameId:long}")]
        [HttpGet]
        public IActionResult GetGameByGameId(long gameId)

        {
            _log.Info($"GET /api/games/{gameId}");
            var result = _mapper.Map<GameDTO>(this._gameService.GetGameById(gameId));
            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetGames([FromQuery(Name = "title")] string title)
        {
            _log.Info($"GET /api/games?title={title}");
            if (title == null)
            {
                return Ok(_mapper.Map<IList<BasicGameDTO>>(this._gameService.GetAllGames()));
            }
            return Ok(_mapper.Map<IList<BasicGameDTO>>(this._gameService.SearchbyString(title)));

        }


        [Produces("application/json")]
        [Route("{gameId:long}")]
        [HttpDelete]
        public IActionResult DeleteGameByGameId(long gameId)
        {
            _log.Info($"DELETE /api/games/{gameId}");
            var result = _mapper.Map<DeleteGameDTO>(this._gameService.DeleteGameById(gameId));
            return Ok(result);
        }

        [Produces("application/json")]
        [Route("{gameId:long}/platforms")]
        [HttpGet]
        public IActionResult GetPlatformsByGameId(long gameId)
        {
            _log.Info($"GET /api/games/{gameId}/platforms");

            var result = _mapper.Map<IList<PlatformDTO>>(this._gameService.GetPlatformsByGameId(gameId));
            return Ok(result);
        }

        [HttpGet("mostpopular")]
        public IActionResult MostPopular([FromQuery(Name = "platform")] long platformId)
        {
            _log.Info($"GET /api/games/mostpopular?platform={platformId}");
            return Ok(
                _mapper.Map<IList<NewestGamesDTO>>(
                    this._gameService.GetMostPopularGames(platformId)
                )
            );
        }

        [HttpGet("newest")]
        public IActionResult Newest([FromQuery(Name = "platform")] long platformId)
        {
            _log.Info($"GET /api/games/newest?platform={platformId}");
            return Ok(
                _mapper.Map<IList<NewestGamesDTO>>(
                    this._gameService.GetNewestGames(platformId)
                )
            );
        }


        [Route("{GameId:long}/details")]
        [HttpGet]
        public IActionResult Details(long gameId)

        {
            _log.Info($"GET /api/games/{gameId}/details");
            var result = _mapper.Map<DetailedGameDTO>(this._gameService.GetDetailedInformation(gameId));
            return Ok(result);
        }

        [Route("subscribe")]
        [HttpPost]
        public IActionResult SubscribeUserToGame([FromBody] SubscribeGameDTO subscribeGameDTO)
        {
            _subscriptionService.Subscribe(subscribeGameDTO.GameId, subscribeGameDTO.UserId);
            return Ok();
        }

        [Route("unsubscribe")]
        [HttpDelete]
        public IActionResult UnSubscribeUserToGame([FromBody] SubscribeGameDTO subscribeGameDTO)
        {
            _subscriptionService.UnSubscribe(subscribeGameDTO.GameId, subscribeGameDTO.UserId);
            return Ok();
        }
    }
}
