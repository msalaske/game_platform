﻿namespace game_platform.Game.Web.Dto
{
    public class DetailedGameDTO 
    {
        public string YouTubeTrailer { get; set; }

        public string Description { get; set; }

        public decimal AverageScore { get; set; }

        public int PublicationYear { get; set; }
    }
}
