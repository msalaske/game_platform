﻿using System;
using System.Collections.Generic;

namespace game_platform.Game.Web.Dto
{
    public class AddGameDTO
    {

        public long Id { get; set; }

        public string Title { get; set; }

        public AddGameDTO()
        {
            PlatformIds = new List<long>();

        }
        public DateTime PublicationDate { get; set; }

        public string Producer { get; set; }

        public string YouTubeTrailer { get; set; }

        public string Description { get; set; }

        public ICollection<long> PlatformIds { get; set; }
    }
}
