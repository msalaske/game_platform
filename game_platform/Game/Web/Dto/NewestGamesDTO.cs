﻿namespace game_platform.Game.Web.Dto
{
    public class NewestGamesDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Producer { get; set; }
    }
}
