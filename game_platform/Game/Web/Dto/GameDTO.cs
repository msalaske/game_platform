﻿using System.Collections.Generic;
using game_platform.Rating.Web.Dto;

namespace game_platform.Game.Web.Dto
{
    public class GameDTO
    {
        public long Id { get; set; }

        public string Title { get; set; }
        public List<RatingDTO> Ratings { get; set; }

        public string Producer { get; set; }

        public List<PlatformDTO> Platforms { get; set;}
    }
}
