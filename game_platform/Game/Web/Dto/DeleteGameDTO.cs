﻿namespace game_platform.Game.Web.Dto
{
    public class DeleteGameDTO
    {
        public long Id { get; set; }
    }
}
