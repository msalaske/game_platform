﻿using System;
using System.Collections.Generic;
using game_platform.Rating;
namespace game_platform.Game
{
    public class Game : BaseModel
    {

        public string Title { get; set; }

        public DateTime PublicationDate { get; set; }

        public Game()
        {
            
            Ratings = new List<Rating.Rating>();
            Platforms = new List<Platform>();
        }
        public string Producer { get; set; }

        public string YouTubeTrailer { get; set; }

        public string Description { get; set; }

        public decimal AverageScore { get; set; }
        public ICollection<Rating.Rating> Ratings { get; set; }

        public ICollection<Platform> Platforms { get; set; }

    }
}
